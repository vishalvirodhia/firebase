//
//  LoginViewTest.swift
//  FireBaseDemoUITests
//
//  Created by Vishal on 04/01/18.
//  Copyright © 2018 Vishal. All rights reserved.
//

import XCTest

class LoginviewUITests: XCTestCase {
    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        
        // Since UI tests are more expensive to run, it's usually a good idea
        // to exit if a failure was encountered
        continueAfterFailure = false
        
        app = XCUIApplication()
        
        // We send a command line argument to our app,
        // to enable it to reset its state
        app.launchArguments.append("--uitesting")
    }
    
    func testGoingThroughOnboarding() {
        app.launch()
        
        // Make sure we're displaying onboarding
        XCTAssertTrue(app.isDisplayingOnboarding)
       
        
        // Tap the "Done" button
        app.buttons["Segue"].tap()
        
        // Onboarding should no longer be displayed
        XCTAssertFalse(app.isDisplayingOnboarding)
    }
}
extension XCUIApplication {
    var isDisplayingOnboarding: Bool {
        return otherElements["onboardingView"].exists
    }
}
