//
//  ViewController.swift
//  FireBase
//
//  Created by Vishal on 29/12/17.
//  Copyright © 2017 Vishal. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

struct SegueIdentifier {
    static let toHome = "toHome"
}


extension UIViewController {
    func showMessage(message: String) {
        BPStatusBarAlert(duration: 0.3, delay: 2, position: .statusBar)
            .message(message: message)
            .messageColor(color: .white)
            .bgColor(color: .black)
            .completion {
                
            }
            .show()
    }
}
