//
//  LoginViewController.swift
//  FireBase
//
//  Created by Vishal on 03/01/18.
//  Copyright © 2018 Vishal. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController, LoginViewProtocols {
    
    @IBOutlet var loginView: LoginView!
    
    private var presenter: LoginViewPresenter!
    var loggedUser: User?
    
    //MARK:= view cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.accessibilityIdentifier = "onboardingView"
        
        loginView.delegate = self
        
        presenterSetup()
    }
    
    //Presenter setup
    private func presenterSetup() {
        self.presenter = LoginViewPresenter(view: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- custom view delegate
    func buttonClicked(tag: ButtonTag) {
        switch tag {
        case .login:
            presenter.loginTapped(data: LoginModel(email: loginView.emailTextField.text!,
                                                   password: loginView.passwordTextField.text!))
            break
        case .register:
            presenter.signUpTapped(data: LoginModel(email: loginView.emailTextField.text!,
                                                    password: loginView.passwordTextField.text!))
            break
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.toHome {
            if let controller = segue.destination as? HomeViewController {
                controller.loggedUser = self.loggedUser
            }
        }
    }
}

class LoginModel {
    var email: String?
    var password: String?
    
    init(email: String, password: String) {
        self.email = email
        self.password = password
    }
}

extension LoginViewController: LoginProtocols {
    func receivedResponse(message: String, user: User?) {
        self.showMessage(message: message)
        guard let user = user else {
            return
        }
        self.loggedUser = user
        print(user)
        performSegue(withIdentifier: SegueIdentifier.toHome, sender: self)
    }
    
    func showError(error: Error?) {
        self.showMessage(message: String(error?.localizedDescription ?? "error"))
    }
    
    
    
}

///Login protocols
protocol LoginProtocols: class  {
    func receivedResponse(message: String, user: User?)
    func showError(error:Error?)
}

//Login Presenter
class LoginViewPresenter {
    
    weak private var view: LoginProtocols?
    
    init(view: LoginProtocols) {
        self.view = view
        
    }
    
    func loginTapped(data:LoginModel) {
        guard let email = data.email , let password = data.password else {
            view?.showError(error: nil)
            return
        }
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if let error = error {
                self.view?.showError(error: error)
                return
            }
            self.view?.receivedResponse(message:"\(user!.email!) logged in", user: user)
            print("\(user!.email!) logged in")
        }
        
    }
    
    func signUpTapped(data:LoginModel) {
        guard let email = data.email , let password = data.password else {
            view?.showError(error: nil)
            return
        }
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if let error = error {
                self.view?.showError(error: error)
                return
            }
            self.view?.receivedResponse(message: "\(user!.email!) created", user: user)
            print("\(user!.email!) created")
        }
    }
}




//////////

///Button emum for action
enum ButtonTag : Int {
    case register = 1
    case login = 2
}

// Login view protocols
protocol LoginViewProtocols {
    func buttonClicked(tag: ButtonTag)
}

///custom class for login view controller
class LoginView: UIView {
    
    var delegate: LoginViewProtocols?
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBAction func buttonAction(_ sender: UIButton) {
        delegate?.buttonClicked(tag: ButtonTag(rawValue: sender.tag)!)
    }
    
}

